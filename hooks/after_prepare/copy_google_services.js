#!/usr/bin/env node

var fs = require('fs');
var path = require('path');

var ROOT_DIR = process.argv[2];
var PLATFORM = process.env.CORDOVA_PLATFORMS;

function copyFileSync(srcFile, target) {
    var destFile = target;

    // If target is a directory a new file with the same name will be created inside it
    if (fs.existsSync(target)) {
        if (fs.lstatSync(target).isDirectory()) {
            destFile = path.join(target, path.basename(srcFile));
        }
    }

    console.log('copying ' + srcFile + ' to ' + destFile);
    fs.writeFileSync(destFile, fs.readFileSync(srcFile));
}

if(PLATFORM == 'android') {
    copyFileSync(path.join(ROOT_DIR, 'deploy/android/google-services.json'), path.join(ROOT_DIR, 'platforms/android'));
}
