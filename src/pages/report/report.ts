import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, ModalController, ToastController, Events } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { ChatPage } from '../chat/chat';
import { UserInfo } from "../../providers/chat-service/chat-service";
import { SelectSearchableComponent } from 'ionic-select-searchable';

@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {

  @ViewChild('customerComponent') customerComponent: SelectSearchableComponent;

  userData = {
    id: "",
    token: "",
    is_admin: false,
    username: "",
    device_token: "",
    refresh_token: "",
    unread_reports: 0
  };

  postData = {
    page: 1,
    type: "report",
    token: "",
    user_id : ""
  };

  reports: any;
  customers: any;

  toUser: UserInfo;

  unreadReports: any;

  constructor(public navCtrl: NavController, public authService: AuthServiceProvider, public loading: LoadingController, public modalCtrl: ModalController, private toastCtrl:ToastController, public events: Events) {
    this.userData = JSON.parse(localStorage.getItem("userData"));
  }

  ionViewDidEnter() {
    this.loadReports();
    this.loadCustomerSelection();
    this.setUnreadMessagesBadge();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  loadReports() {
    let loading = this.loading.create();
    loading.present();
    this.postData.token = this.userData.token;
    this.postData.user_id = this.userData.id;
    this.authService.postData(this.postData, "reports").then((result) =>{
      if(result) {
        this.reports = result;
        // Get unread reports
        this.setUnreadMessagesBadge();
      }
      else{
        console.log("Failed getting reports");
      }
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    }, (err) => {
      console.log("Connection failed");
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    });
  }

  loadCustomerSelection() {
    // Get customers without report already created
    this.authService.postData({token: this.userData.token}, "users-report").then((result) =>{
      if(result) {
        this.customers = result;
      }
      else{
        console.log("Failed getting customers");
      }
    }, (err) => {
      console.log("Connection failed");
    });
  }

  setUnreadMessagesBadge() {
    this.postData.token = this.userData.token;
    this.postData.user_id = this.userData.id;
    this.authService.postData(this.postData, "unread-reports").then((result) =>{
      let unreadReportsResult: any = result;
      this.unreadReports = unreadReportsResult.result.unreadReports;
      for(var i = 0; i < this.reports.length; i++) {
        this.reports[i].unread = 0;
        this.reports[i].unread = this.unreadReports[this.reports[i].client_id];
      }
    }, (err) => {
      console.log("Connection failed");
    });
  }

  showReport(report) {
    let customer = {
      id: report.client_id,
      username: report.username
    }
    this.navCtrl.push(ChatPage, { 'customer': customer });
  }

  hasPermission() {
    if(this.userData)
      return this.userData.is_admin;
    else
      return false;
  }

  newReport(){
    this.customerComponent.open();
  }

  createReport(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
    this.customerComponent.clear();
    this.navCtrl.push(ChatPage, { 'customer': event.value });
  }
}
