import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Platform, NavController, NavParams, Events, Content, ActionSheetController, normalizeURL, PopoverController, ToastController } from 'ionic-angular';
import { ChatService, ChatMessage, UserInfo } from "../../providers/chat-service/chat-service";
import { DomSanitizer } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { SchedulePage } from '../schedule/schedule';
import { GalleryPage } from '../gallery/gallery';
import { ImageViewerController } from 'ionic-img-viewer';
import { ChatPopoverPage } from '../popovers/chatPopoverPage';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: ElementRef;

  userData = {
    id: "",
    token: "",
    is_admin: false,
    username: "",
    device_token: "",
    refresh_token: "",
    unread_chats: 0
  };

  postData = {
    page: 1,
    type: "report",
    token: "",
    user_id: "",
    username: "",
    reader_id: "",
    messages_id: "",
    report_id: ""
  };

  toUser: UserInfo;
  msgList: ChatMessage[] = [];
  editorMsg = '';
  imageMsg = '';

  scrollDirection: any;
  messagesToMarkAsViewed = [];

  receiving = false;

  constructor(public platform: Platform, private zone: NgZone, public navCtrl: NavController,
    public navParams: NavParams, private chatService: ChatService, private events: Events,
    private _DomSanitizationService: DomSanitizer, private camera: Camera,
    public actionSheetCtrl: ActionSheetController, public authService: AuthServiceProvider,
    public imageViewerCtrl: ImageViewerController, public popoverCtrl: PopoverController,
    private toastCtrl:ToastController) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userData = data;
    let customerParam = this.navParams.get('customer');
    // Get the navParams toUserId parameter
    if(customerParam) {
      this.toUser = customerParam;
    }
    else {
      this.toUser = {
        id: '88',
        username: 'Administrador'
      };
    }
  }

  ngOnInit() {
  }

  ionViewWillLeave() {
    // unsubscribe
    this.events.unsubscribe('chat:received');
  }

  ionViewDidEnter() {
    // Get message list
    this.getMsg();

    // Subscribe to received  new message events
    this.events.subscribe('chat:received', msg => {
      this.zone.run(() => {
        this.receiving = true;
        this.scrollToBottom(10);
        if(msg.image == "1") {
          this.postData.report_id = msg.id;
          this.authService.postData(this.postData, "get-image-for-report").then((result) =>{
            let msgImage: any = result;
            if(msgImage) {
              msg.image = msgImage.image;
            }
            else{
              console.log("Failed getting image for report");
            }
          }, (err) => {
            console.log("Connection failed getting image for report");
          });
        }
        setTimeout(() => {
          this.pushNewMsg(msg);
        }, 1000);
      });
    });
  }

  onFocus() {
    this.content.resize();
    this.scrollToBottom(400);
  }

  getMsg() {
    this.postData.token = this.userData.token;
    if(this.userData.is_admin)
      this.postData.user_id = this.toUser.id;
    else
      this.postData.user_id = this.userData.id;
    this.getMessages(this.postData, 1, null);
  }

  /**
   * @name getMessages
   * @returns {Promise<ChatMessage[]>}
   */
  getMessages(postData, page, infiniteScroll) {
    this.messagesToMarkAsViewed = [];
    let result = this.chatService.getMsgList(postData).subscribe(res => {
      if(page > 1) {
        for (var i = 0; i < res.length; i++) {
          if(!res[i].viewed) {
            this.messagesToMarkAsViewed.push(res[i].id);
          }
          this.msgList.unshift( res[i] );
        }
        infiniteScroll.complete();
      }
      else {
        this.msgList = res.reverse();
        for (var i = 0; i < res.length; i++) {
          if(!res[i].viewed) {
            this.messagesToMarkAsViewed.push(res[i].id);
          }
        }
        this.scrollToBottom(400);
      }
      this.markAsViewed();
    });
    return result;
  }

  markAsViewed(all?: boolean) {
    this.postData.reader_id = this.userData.id;
    if(all != undefined && all) {
      this.postData.user_id = this.toUser.id;
      this.postData.messages_id = "all";
      this.markAsViewedApiCall(all);
    } else if(this.messagesToMarkAsViewed.length) {
      this.postData.messages_id = this.messagesToMarkAsViewed.join(',');
      this.markAsViewedApiCall();
    }
  }

  markAsViewedApiCall(all?: boolean) {
    this.authService.postData(this.postData, "mark-as-view-report-msg").then((result) =>{
        if(result) {
          this.messagesToMarkAsViewed = [];
          this.events.publish('user:info');
          if(all != undefined && all) {
            this.presentToast("Todos los mensajes han sido marcados como leídos");
          }
        }
        else{
          console.log("Failed getting reports");
        }
      }, (err) => {
        console.log("Connection failed");
      });
  }

  /**
   * @name sendMsg
   */
  sendMsg() {
    if (!this.editorMsg.trim() && !this.imageMsg) return;

    const id = Date.now().toString();
    let newMsg: ChatMessage = {
      id: id,
      client_id: (this.userData.is_admin)? this.toUser.id : this.userData.id,
      sender_id: this.userData.id,
      sender_name: this.userData.username,
      sender_admin: (this.userData.is_admin)? 1 : 0,
      sender_avatar: 'assets/imgs/logo.png',
      receiver_id: this.toUser.id,
      receiver_name: this.toUser.username,
      receiver_admin: (this.userData.is_admin)? 0: 1,
      send: Date.now().toString(),
      message: this.editorMsg,
      image: this.imageMsg,
      status: 'pending',
      viewed: '',
      erased: false
    };

    this.pushNewMsg(newMsg);
    this.editorMsg = '';
    this.imageMsg = '';

    this.chatService.sendMsg(newMsg)
      .then((result) => {
        let sendResult : any = result;
        let index = this.getMsgIndexById(id);
        if (index !== -1) {
          this.msgList[index].send = sendResult.send;
          this.msgList[index].status = 'success';
        }
        this.scrollToBottom(400);
      })
  }

  /**
   * @name pushNewMsg
   * @param msg
   */
  pushNewMsg(msg: ChatMessage) {
    const senderId = this.userData.id;
    const receiverId = this.toUser.id;
    // Verify user relationships
    // This case if on send message
    if (msg.sender_id === senderId) {
      this.msgList.push(msg);
    } else {
      // On this case if message receiver it's same that user logged
      // or user logged is admin and chat opened belongs to sender user
      if (msg.receiver_id === this.userData.id || (this.userData.is_admin && msg.sender_id === this.toUser.id) ) {
        this.zone.run(() => {
          this.msgList.push(msg);
          this.receiving = false;
          this.messagesToMarkAsViewed.push(msg.id);
          this.markAsViewed();
        });
      }
    }
    this.scrollToBottom(400);
  }

  getMsgIndexById(id: string) {
    return this.msgList.findIndex(e => e.id === id)
  }

  scrollToBottom(time = 400) {
    setTimeout(() => {
      if (this.content._scroll) {
        this.content.scrollToBottom(400);
      }
    }, time)
  }

  onScroll($event) {
    if($event) {
      this.scrollDirection = $event.directionY;
    }
    return this.scrollDirection;
  }

  doInfinite(infiniteScroll) {
    if(this.scrollDirection == 'up') {
      this.scrollDirection = 'down';
      this.postData.page += 1;
      this.getMessages(this.postData, this.postData.page, infiniteScroll);
    } else {
      setTimeout(() => {
        infiniteScroll.complete();
      }, 500);
    }
  }

  hasPermission() {
    if(this.userData)
      return this.userData.is_admin;
    else
      return false;
  }

  sendImg() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Galería',
          icon: 'images',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Cámara',
          icon: 'camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          icon: 'close',
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 10,
      targetHeight: 300,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: (this.platform.is('ios'))? this.camera.EncodingType.JPEG : this.camera.EncodingType.PNG
    };
   
    // Get the data of an image
    this.camera.getPicture(options)
      .then((imageData) => {
        if (this.platform.is('ios'))
          this.imageMsg = 'data:image/jpeg;base64,' + normalizeURL(imageData);
        else
          this.imageMsg = 'data:image/jpeg;base64,' + imageData;
        this.sendMsg();
      }, (err) => {
        // Handle error
        console.log("Camera get picture error " + err);
    });
  }

  goBack(){
    if(!this.hasPermission()) {
      this.navCtrl.setRoot(SchedulePage);
    }
  }

  gallery() {
    this.navCtrl.push(GalleryPage, { 'customer': this.toUser });
  }

  presentImage(myImage) {
    const imageViewer = this.imageViewerCtrl.create(myImage);
    imageViewer.present();

    setTimeout(() => imageViewer.dismiss(), 1000);
    imageViewer.onDidDismiss(() => alert('Viewer dismissed'));
  }

  openChatPopover(event, messageId, isErased) {
    if(!isErased) {
      let popover = this.popoverCtrl.create(ChatPopoverPage, { 'id': messageId });
      popover.present({
        ev: event
      });
      popover.onDidDismiss(data => {
        if(data) {
          let index = this.getMsgIndexById(messageId);
          if (index !== -1) {
            this.msgList[index].erased = true;
            this.msgList[index].image = '';
            this.msgList[index].message = 'Este mensaje ha sido borrado';
          }
          this.presentToast("Mensaje borrado correctamente");
        }
      });
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  pageOptions() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Marcar todo como leído',
          handler: () => {
            this.markAsViewed(true);
            this.events.publish('user:info');
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          icon: 'close',
        }
      ]
    });
    actionSheet.present();
  }

}
