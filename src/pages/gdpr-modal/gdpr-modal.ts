import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController, Platform } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";

@IonicPage()
@Component({
  selector: 'page-gdpr-modal',
  templateUrl: 'gdpr-modal.html',
})
export class GdprModalPage {

	gdpr = {
    title: '',
    body: '',
    authorization_token: ''
  };

  private backButtonUnregister: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public loading: LoadingController, public authService: AuthServiceProvider, private toastCtrl:ToastController, public platform: Platform) {
  	let gdprParam = this.navParams.get('authorization');
    this.gdpr = gdprParam;
    this.backButtonUnregister = platform.registerBackButtonAction(() => {});
  }

  ionViewDidLoad() {}

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  ionViewWillLeave() {
    this.backButtonUnregister();
  }

  authorize() {
    let loading = this.loading.create();
    loading.present();
    let postData = {
      token : this.gdpr.authorization_token
    }
    this.authService.postData(postData, "accept_authorization").then((result) => {
      if(result) {
        this.viewCtrl.dismiss(this.gdpr);
        setTimeout(() => {
          loading.dismiss();
        }, 1000);
      }
      else{
        console.log("Failed");
        setTimeout(() => {
          loading.dismiss();
        }, 1000);
      }
    }, (err) => {
      console.log("Connection failed");
      this.presentToast("Error al acceptar la autorización");
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    });
  }
}
