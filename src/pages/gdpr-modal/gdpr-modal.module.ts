import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GdprModalPage } from './gdpr-modal';

@NgModule({
  declarations: [
    GdprModalPage,
  ],
  imports: [
    IonicPageModule.forChild(GdprModalPage),
  ],
})
export class GdprModalPageModule {}
