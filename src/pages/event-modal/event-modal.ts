import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import moment from 'moment';
import { ModalController } from 'ionic-angular';
import { DatePickerProvider, DatePickerOption } from 'ionic2-date-picker';

@IonicPage()
@Component({
  selector: 'page-event-modal',
  templateUrl: 'event-modal.html',
})
export class EventModalPage {
  userData = {
    id: "",
    token: "",
    is_admin: false,
    refresh_token: "",
  };
  customers: any;
  event = {
    id: '',
    user_id: '',
    allDay: false,
    customer: '',
    duration: 60,
    start_date: '',
    start_date_showed: '',
    start_time: '',
    isEdit: false,
    attendance: true,
  };
  // Form variables
  eventForm: FormGroup;
  submitted: boolean = false;
  responseData: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loading: LoadingController,
    public authService: AuthServiceProvider,
    private alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    private datePickerProvider: DatePickerProvider,
    public modalCtrl: ModalController
  ) {
    let eventParam = this.navParams.get('event');
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userData = data;
    this.event = eventParam;
    this.event.id         = eventParam.id;
    this.event.user_id    = this.userData.id;
    this.event.isEdit     = (eventParam.id)? true : false;
    this.event.attendance = eventParam.attendance;
    this.event.customer   = eventParam.customer;
    this.event.duration   = eventParam.duration;
    this.event.start_date = moment(eventParam.startTime).format('YYYY-MM-DD');
    this.event.start_date_showed = moment(eventParam.startTime).format('DD/MM/YYYY');
    let startTimeMinutes = moment(eventParam.startTime).minutes();
    let startTimeFormat = (startTimeMinutes >= 30)? 'HH:30' : 'HH:00';
    this.event.start_time = moment(eventParam.startTime).format(startTimeFormat);
    // Prepare form validation
    this.eventForm = formBuilder.group({
      customer: ['', Validators.compose([Validators.required])],
      start_date: ['', Validators.compose([Validators.required])],
      start_time: ['', Validators.compose([Validators.required])],
      duration: ['', Validators.compose([Validators.required])],
      attendance: [''],
      observation: [''],
    });

    // Get customers
    this.authService.postData({token: this.userData.token}, "customers").then((result) =>{
      if(result) {
        this.customers = result;
      }
      else{
        console.log("Failed getting customers");
      }
    }, (err) => {
      console.log("Connection failed");
    });
  }

  showCalendar() {
    let datePickerOption: DatePickerOption = {
      minimumDate: new Date(), // the minimum date selectable
      locale: 'es'
    };
    const dateSelected = this.datePickerProvider.showCalendar(this.modalCtrl, datePickerOption);

    dateSelected.subscribe(date =>
      this.setStartDate(date)
    );
  }

  ionViewDidLoad() {
    this.setStartDate(this.event.start_date);
  }

  isReaonly() {
    return this.event.isEdit;
  }

  setStartDate(startDate) {
    this.event.start_date = startDate;
    let startDateLocale = moment(startDate);
    startDateLocale.locale('es');
    this.event.start_date_showed = startDateLocale.format('dddd D MMMM YYYY');
    return moment(startDate).format('DD/MM/YYYY');
  }

  hasPermission() {
    if(this.userData)
      return this.userData.is_admin;
    else
      return false;
  }

  confirmDelete() {
    let alert = this.alertCtrl.create({
      title: '¿Estás segur@?',
      message: 'Borrar la sesion',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            console.log('Confirm clicked');
            this.delete();
          }
        }
      ]
    });
    alert.present();
  }

  cancel() {
  	this.event = {
      id: '',
      user_id: '',
      allDay: false,
      customer: '',
      duration: 60,
      start_date: '',
      start_date_showed: '',
      start_time: '',
      isEdit: false,
      attendance: true,
    };
    this.viewCtrl.dismiss({});
  }

  save() {
    this.submitted = true;
    let loading = this.loading.create();
    if(this.eventForm.valid) {
      // Prepare dates
      this.event.start_date = moment(this.event.start_date).format('YYYY-MM-DD');
      loading.present();
      this.authService.postData(this.event, "event").then((result) =>{
        this.responseData = result;
        if(this.responseData) {
          if(this.responseData.result == true) {
            this.viewCtrl.dismiss(true);
          }
          else {
            this.viewCtrl.dismiss(this.responseData);
          }
          setTimeout(() => {
            loading.dismiss();
          }, 1000);
        }
        else{
          console.log("Failed");
          setTimeout(() => {
            loading.dismiss();
          }, 1000);
        }
      }, (err) => {
        console.log("Connection failed");
        setTimeout(() => {
          loading.dismiss();
        }, 1000);
      });
    }
  }

  delete() {
    let loading = this.loading.create();
    loading.present();
    this.authService.postData({id: this.event.id, user_id: this.userData.id}, "event-delete").then((result) =>{
      if(result) {
        this.viewCtrl.dismiss('deleted');
      }
      else{
        console.log("Failed");
      }
      loading.dismiss();
    }, (err) => {
      console.log("Connection failed");
      loading.dismiss();
    });
  }
}
