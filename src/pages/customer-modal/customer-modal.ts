import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-customer-modal',
  templateUrl: 'customer-modal.html',
})
export class CustomerModalPage {
  userData = {
    id: "",
    token:    "",
    is_admin: false,
    refresh_token: "",
  };
  customer = {
    id:       '',
    username: '',
    timetable: [],
    isEdit: false,
    anniversary: new Date().toISOString(),
    active: true,
  };
  // Form variables
  customerForm: FormGroup;
  submitted: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loading: LoadingController,
    public authService: AuthServiceProvider,
    private alertCtrl: AlertController,
    private toastCtrl:ToastController,
    public formBuilder:FormBuilder
  ) {
    let customerParam = this.navParams.get('customer');
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userData = data;
    this.customer = customerParam;
    this.customer.isEdit = (customerParam.id)? true : false;
    this.customer.active = (customerParam.id)? customerParam.active : true;
    this.customer.anniversary = (customerParam.id)? customerParam.anniversary : new Date().toISOString();
    this.customerForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      surname: [''],
      email: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required])],
      anniversary: [''],
      birthday: [''],
      rate: [''],
      active: [''],
      appEnabled: [''],
      hour: [''],
      reported: ['']
    });
  }

  ionViewDidLoad() {}

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  isReaonly() {
    return this.customer.isEdit;
  }

  isSelected(days, day) {
    if (days.indexOf(day) != -1) {
      return true;
    }
    else {
      return false
    }
  }

  confirmDelete() {
    let alert = this.alertCtrl.create({
      title: '¿Estás segur@?',
      message: 'Borrar al Usuario',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            console.log('Confirm clicked');
            this.delete();
          }
        }
      ]
    });
    alert.present();
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  save() {
    this.submitted = true;
    if(this.customerForm.valid) {
      let loading = this.loading.create();
      loading.present();
      let postData = {
        customer : JSON.stringify(this.customer)
      }
      this.authService.postData(postData, "customer").then((result) =>{
        if(result) {
          this.viewCtrl.dismiss(this.customer);
          setTimeout(() => {
            loading.dismiss();
          }, 1000);
        }
        else{
          console.log("Failed");
          setTimeout(() => {
            loading.dismiss();
          }, 1000);
        }
      }, (err) => {
        console.log("Connection failed");
        this.presentToast("Error al crear el usuario, compruebe que el nombre de usuario y correo electrónico no existan anteriormente");
        setTimeout(() => {
          loading.dismiss();
        }, 1000);
      });
    }
  }

  delete() {
    this.authService.postData({id: this.customer.id}, "customer-delete").then((result) =>{
      if(result) {
        this.viewCtrl.dismiss('deleted');
      }
      else{
        console.log("Failed");
      }
    }, (err) => {
      console.log("Connection failed status" + err.status + " msg: " + err.statusText);
      this.presentToast("El usuario no se ha podido eliminar");
    });
  }
}
