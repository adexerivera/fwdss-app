import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController, ToastController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { GdprModalPage } from '../gdpr-modal/gdpr-modal';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import moment from 'moment';
import { Device } from '@ionic-native/device';

@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html'
})
export class SchedulePage {
  userData = {
    id: "",
    token: "",
    is_admin: false,
    username: "",
    device_token: "",
    refresh_token: "",
  };
  eventSource: any;
  viewTitle;
  isToday: boolean;
  calendar = {
    mode: 'day',
    currentDate: new Date(),
    locale: 'es'
  }; // these are the variable used by the calendar.
  responseData: any;
  postData = {
    token: "",
    refresh_token: "",
    start: "",
    end: "",
    customer : ''
  };
  googleSendData = {
    key: 'AIzaSyC4_HQiHUsSnZx0INN5D-BmXNR9SzmNNDQ',
    singleEvents: true,
    timeMin: '',
    timeMax: '',
    alwaysIncludeEmail: false,
  }
  selectedDay = new Date();

  constructor(public navCtrl: NavController, public authService: AuthServiceProvider, public modalCtrl: ModalController, public loading: LoadingController, private toastCtrl:ToastController, private device: Device) {
    const data = JSON.parse(localStorage.getItem("userData"));
    if(!data) {
      this.navCtrl.setRoot(LoginPage);
    }
    else {
      this.userData = data;
      this.linkDeviceUser();
      this.authorizationGdpr();
    }
  }

  hasPermission() {
    if(this.userData)
      return this.userData.is_admin;
    else
      return false;
  }

  loadEvents() {
    let loading = this.loading.create();
    loading.present();
    this.authService.getEvents(this.postData, this.googleSendData)
      .subscribe(events => {
        let scheduleEvents: any = events[0];
        let googleEvents: any = events[1];
        this.eventSource = scheduleEvents.concat(googleEvents);
        setTimeout(() => {
          loading.dismiss();
        }, 1000);
      }, error => {
        console.log( error );
        setTimeout(() => {
          loading.dismiss();
        }, 1000);
      });
  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
      if(!this.userData.is_admin) {
        this.calendar.mode = 'month';
      }
  }

  onEventSelected(event) {
    let modal = this.modalCtrl.create('EventModalPage', {event: event});
    modal.present();
    modal.onDidDismiss(data => {
      if(data) {
        if(data == 'deleted') {
          this.presentToast("Sesión borrada correctamente");
        }
        else {
          if(data === true)
            this.presentToast("Sesión actualizada correctamente");
        }
        this.loadEvents();
      }
    });
  }

  changeMode(mode) {
    this.calendar.mode = mode;
  }

  today() {
    this.calendar.currentDate = new Date();
  }

  onTimeSelected(ev) {
    // console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' + (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
    this.selectedDay = ev.selectedTime;
  }

  onCurrentDateChanged(event:Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
  }

  onRangeChanged(ev) {
    if(!this.userData.token) {
      this.navCtrl.setRoot(LoginPage);
    }
    else {
      var iniDate = moment(ev.startTime);
      let endDate;
      if(this.calendar.mode == 'day')
        endDate = moment(ev.startTime);
      else
        endDate = moment(ev.endTime);
      this.postData.start = iniDate.format('YYYY-MM-DD');
      this.googleSendData.timeMin = iniDate.format('YYYY-MM-DD') + 'T00:00:00Z';
      this.postData.end = endDate.format('YYYY-MM-DD');
      this.googleSendData.timeMax = endDate.format('YYYY-MM-DD') + 'T23:59:59Z';
      // Filter user events
      if(!this.userData.is_admin)
        this.postData.customer = this.userData.id;
      this.loadEvents();
    }
  }

  markDisabled = (date:Date) => {
    var current = new Date();
    current.setHours(0, 0, 0);
    return date < current;
  };

  addEvent() {
    let modal = this.modalCtrl.create('EventModalPage', {event: {startTime: this.selectedDay.toISOString(), duration: 60, attendance: true}});
    modal.present();
    modal.onDidDismiss(data => {
      if(data.result) {
        this.loadEvents();
        this.presentToast("Sesión creada correctamente");
      }
      else {
        this.loadEvents();
        this.presentToast(data.message);
      }
    });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  linkDeviceUser() {
    if(this.userData.device_token == null) {
      let sendData = {
        uuid: this.device.uuid,
        username: this.userData.username,
      };
      this.authService.postData(sendData, "link-device").then((result) =>{
        let linkDeviceResponse: any = result;
        if(linkDeviceResponse.result) {
          this.userData.device_token = linkDeviceResponse.token;
          localStorage.setItem('userData', JSON.stringify(this.userData));
        }
      }, (err) => {
        console.log("Connection failed on link device user");
      });
    }
  }

  isHoliday(events) {
    for(var i = 0; i < events.length; i++) {
      if(events[i].type == 'holiday')
        return 'has-holiday';
    }
    return '';
  }

  authorizationGdpr() {
    let sendData = {
      authorization_id: 3,
      customer_id: this.userData.id
    };
    let gdpr: any;
    this.authService.postData(sendData, "check-gdpr").then((result) => {
      gdpr = result;
      if(!gdpr.accepted) {
        let modal = this.modalCtrl.create(GdprModalPage, {authorization: gdpr});
        modal.present();
      }
    }, (err) => {
      console.log("Connection failed");
    });
  }

}
