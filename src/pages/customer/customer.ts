import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController, ToastController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";

@Component({
  selector: 'page-customer',
  templateUrl: 'customer.html'
})
export class CustomerPage {
  userData = {
    id: "",
    token: "",
    is_admin: false,
  };
  selectedCustomer: any;
  customers: any;
  responseData: any;
  postData = {
    token: "",
    refresh_token: ""
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthServiceProvider, public loading: LoadingController, public modalCtrl: ModalController, private toastCtrl:ToastController) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userData = data;
    this.selectedCustomer = navParams.get('customer');
    this.loadCustomers();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  loadCustomers() {
    let loading = this.loading.create();
    loading.present();
    this.postData.token = this.userData.token;
    this.authService.postData(this.postData, "all-customers").then((result) =>{
      if(result) {
        this.customers = result;
      }
      else{
        console.log("Failed getting customers");
      }
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    }, (err) => {
      console.log("Connection failed");
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    });
  }

  addCustomer() {
    let modal = this.modalCtrl.create('CustomerModalPage', {customer: {}});
    modal.present();
    modal.onDidDismiss(data => {
      if(data) {
        this.loadCustomers();
        this.presentToast("Usuario creado correctamente");
      }
    });
  }

  customerEdit(customer) {
    let modal = this.modalCtrl.create('CustomerModalPage', {customer: customer});
    modal.present();
    modal.onDidDismiss(data => {
      if(data) {
        if(data == 'deleted') {
          this.presentToast("Usuario borrado correctamente");
        }
        else {
          this.presentToast("Usuario actualizado correctamente");
        }
        this.loadCustomers();
      }
    });
  }
}
