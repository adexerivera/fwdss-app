import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController, Events, ModalController } from 'ionic-angular';
import { SchedulePage } from '../schedule/schedule';
import { SupportModalPage } from '../support-modal/support-modal';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Device } from '@ionic-native/device';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  responseData : any;
  userData = {
    id: "",
    uuid: "",
    is_admin: false,
    username: "",
    password: "",
  };
  // Form variables
  loginForm: FormGroup;
  submitted: boolean = false;

  constructor(public navCtrl: NavController, public authService: AuthServiceProvider, private toastCtrl:ToastController, public loading: LoadingController, public formBuilder: FormBuilder, private device: Device, public events: Events, public modalCtrl: ModalController) {
    if(localStorage.getItem('userData')){
      this.navCtrl.setRoot(SchedulePage);
    }
    this.loginForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {}

  login() {
    this.submitted = true;
    let loading = this.loading.create();
    if(this.loginForm.valid) {
      if(this.userData.username && this.userData.password) {
        loading.present();
        this.userData.uuid = this.device.uuid;
        // this.authService.postData(this.userData, "login_check").then((result) =>{
        this.authService.postData(this.userData, "login").then((result) =>{
          this.responseData = result;
          if(this.responseData.token) {
            localStorage.setItem('userData', JSON.stringify(this.responseData));
            this.events.publish('user:info');
            this.navCtrl.setRoot(SchedulePage);
            setTimeout(() => {
              loading.dismiss();
            }, 1000);
          }
          else{
            setTimeout(() => {
              loading.dismiss();
            }, 1000);
            this.presentToast("Por favor, inserte nombre de usuario y contraseña válidos");
          }
        }, (err) => {
          setTimeout(() => {
            loading.dismiss();
          }, 1000);
          // Connection failed message
          this.presentToast(err);
        });
      }
      else{
        this.presentToast("Por favor, inserte nombre de usuario y contraseña");
      }
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  sendInfo() {
    let modal = this.modalCtrl.create(SupportModalPage);
    modal.present();
    modal.onDidDismiss(data => {
      if(data) {
        this.presentToast("Información enviada correctamente");
      }
    });
  }

}
