import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ToastController } from 'ionic-angular';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  profile = {
    id: ''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, private toastCtrl:ToastController) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.profile = data;
  }

  isSelected(days, day) {
    if (days.indexOf(day) != -1) {
      return true;
    }
    else {
      return false
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  changePassword() {
    let modal = this.modalCtrl.create('ChangePasswordModalPage');
    modal.present();
    modal.onDidDismiss(data => {
      if(data) {
        this.presentToast("Contraseña modificada correctamente");
      }
    });
  }

}
