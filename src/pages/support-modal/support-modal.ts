import { Component } from '@angular/core';
import { Platform, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { Device } from '@ionic-native/device';

@Component({
  selector: 'page-support-modal',
  templateUrl: 'support-modal.html',
})
export class SupportModalPage {
	support = {
		email: '',
		subject: '',
		description: ''
	}
  userData = {
    id: "",
    email: "",
    token: "",
    is_admin: false,
    refresh_token: ""
  };

  // Form variables
  supportForm: FormGroup;
  submitted: boolean = false;

  constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public loading: LoadingController, public authService: AuthServiceProvider, public formBuilder:FormBuilder, private toastCtrl:ToastController, private device: Device) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userData = data;
    this.support.email = (this.userData)? this.userData.email : '';
  	this.supportForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      subject: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportModalPage');
  }

  isReaonly() {
    return (this.userData)? true : false;
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  send() {
  	this.submitted = true;
    if(this.supportForm.valid) {
      let loading = this.loading.create();
      let deviceInfo;
      loading.present();
      if (this.platform.is('cordova')) {
        deviceInfo = {
          manufacturer: this.device.manufacturer,
          model: this.device.model,
          platform: this.device.platform,
          uuid: this.device.uuid,
          version: this.device.version
        };
      }
      else{
        deviceInfo = {
          manufacturer: 'ADEXE',
          model: 'DEVELOPMENT',
          platform: 'NAVIGATOR',
          uuid: 'UUII',
          version: 'VERSION'
        };
      }
      let postData = {
        form: JSON.stringify({
          email: this.support.email,
          subject: this.support.subject,
          description: this.support.description,
          deviceInfo: deviceInfo
        })
      };
      this.authService.postData(postData, "support").then((result) =>{
        if(result) {
          this.viewCtrl.dismiss(this.support);
          setTimeout(() => {
            loading.dismiss();
          }, 1000);
        }
        else{
          console.log("Failed");
          setTimeout(() => {
            loading.dismiss();
          }, 1000);
        }
      }, (err) => {
        console.log("Connection failed");
        this.presentToast("Error al enviar la información");
        setTimeout(() => {
          loading.dismiss();
        }, 1000);
      });
    }
  }
}
