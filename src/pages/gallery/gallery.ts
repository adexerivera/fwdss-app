import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { ImageViewerController } from 'ionic-img-viewer';
import { UserInfo } from "../../providers/chat-service/chat-service";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { ChatPage } from '../chat/chat';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})
export class GalleryPage {

  userData = {
    id: "",
    token: "",
    is_admin: false,
    username: "",
    device_token: "",
    refresh_token: "",
    unread_chats: 0
  };

  postData = {
    page: 1,
    type: "report",
    token: "",
    user_id: ""
  };

  toUser: UserInfo;
  images: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private authService: AuthServiceProvider,
    public imageViewerCtrl: ImageViewerController, public loading: LoadingController, private _DomSanitizationService: DomSanitizer) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userData = data;
    this.toUser = this.navParams.get('customer');
    this.loadGallery();
  }

  ionViewDidLoad() {}

  loadGallery() {
    let loading = this.loading.create();
    loading.present();
    this.postData.token = this.userData.token;
    this.postData.user_id = this.toUser.id;
    this.authService.postData(this.postData, "report-gallery").then((result) =>{
      if(result) {
        this.images = result;
      }
      else{
        console.log("Failed getting images");
      }
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    }, (err) => {
      console.log("Connection failed");
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    });
  }

  doInfinite(infiniteScroll) {
    this.postData.page += 1;
    this.authService.postData(this.postData, "report-gallery").then((result) =>{
      let galleryList : any = result;
      if(result) {
        for (var i = 0; i < galleryList.length; i++) {
          this.images.push( galleryList[i] );
        }
        infiniteScroll.complete();
      }
      else{
        console.log("Failed getting images");
      }
    }, (err) => {
      console.log("Connection failed");
    });
  }

  goBack(){
    this.navCtrl.push(ChatPage, { 'customer': this.toUser });
  }

  presentImage(imageToView) {
    const imageViewer = this.imageViewerCtrl.create(imageToView._img);
    imageViewer.present();
  }

}
