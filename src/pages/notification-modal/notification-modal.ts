import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController, ToastController, Events } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";

@IonicPage()
@Component({
  selector: 'page-notification-modal',
  templateUrl: 'notification-modal.html',
})
export class NotificationModalPage {

  notification : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public loading: LoadingController, public authService: AuthServiceProvider, private alertCtrl: AlertController, private toastCtrl:ToastController, public events: Events) {
    this.notification = this.navParams.get('notification');
  }

  ionViewDidLoad() {
    this.markAsViewed();
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  confirmDelete() {
    let alert = this.alertCtrl.create({
      title: '¿Estás segur@?',
      message: 'Borrar la notificación',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            console.log('Confirm clicked');
            this.delete();
          }
        }
      ]
    });
    alert.present();
  }

  markAsViewed() {
    if(!this.notification.viewed) {
      let postData = {
        notification : JSON.stringify({id: this.notification.id, viewed: new Date().toISOString()})
      }
      this.authService.postData(postData, "notification-viewed").then((result) =>{
        if(result) {
          this.events.publish('user:info');
        }
        else{
          console.log("Failed");
        }
      }, (err) => {
        console.log("Connection failed status" + err.status + " msg: " + err.statusText);
      });
    }
  }

  delete() {
    this.authService.postData({id: this.notification.id}, "notification-delete").then((result) =>{
      if(result) {
        this.events.publish('user:info');
        this.viewCtrl.dismiss('deleted');
      }
      else{
        console.log("Failed");
      }
    }, (err) => {
      console.log("Connection failed status" + err.status + " msg: " + err.statusText);
      this.presentToast("La notificación no se ha podido eliminar");
    });
  }

}
