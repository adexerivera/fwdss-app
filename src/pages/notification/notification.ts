import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, ToastController, Events, ActionSheetController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  userData = {
    id: "",
    token: "",
    is_admin: false,
    username: "",
    device_token: "",
    refresh_token: "",
    unread_reports: 0,
    unread_notifications: 0,
  };

  postData = {
    page: 1,
    token: "",
    username : "",
    type: "notification"
  };

  notifications: any;

  constructor(public navCtrl: NavController, public authService: AuthServiceProvider,
    public loading: LoadingController, public modalCtrl: ModalController,
    private toastCtrl:ToastController, public events: Events,
    public actionSheetCtrl: ActionSheetController) {
    this.userData = JSON.parse(localStorage.getItem("userData"));
    this.loadNotifications();
  }

  ionViewDidLoad() {}

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  loadNotifications() {
    let loading = this.loading.create();
    loading.present();
    this.postData.token = this.userData.token;
    this.postData.username = this.userData.username;
    this.authService.postData(this.postData, "notifications").then((result) =>{
      if(result) {
        this.notifications = result;
      }
      else{
        console.log("Failed getting notifications");
      }
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    }, (err) => {
      console.log("Connection failed");
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    });
  }

  doInfinite(infiniteScroll) {
    this.postData.page += 1;
    this.authService.postData(this.postData, "notifications").then((result) =>{
      let notificationList : any = result;
      if(result) {
        for (var i = 0; i < notificationList.length; i++) {
          this.notifications.push( notificationList[i] );
        }
        infiniteScroll.complete();
      }
      else{
        console.log("Failed getting notifications");
      }
    }, (err) => {
      console.log("Connection failed");
    });
  }

  showNotification(notification) {
    let modal = this.modalCtrl.create('NotificationModalPage', {notification: notification});
    modal.present();
    modal.onDidDismiss(data => {
      if(data) {
        if(data == 'deleted') {
          this.presentToast("Notification borrada correctamente");
          this.events.publish('user:info');
        }
      }
      this.postData.page = 1;
      this.loadNotifications();
    });
  }

  pageOptions() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Marcar todo como leído',
          handler: () => {
            this.markAsRead();
            this.events.publish('user:info');
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          icon: 'close',
        }
      ]
    });
    actionSheet.present();
  }

  markAsRead() {
    let loading = this.loading.create();
    loading.present();
    this.postData.token = this.userData.token;
    this.postData.username = this.userData.username;
    this.authService.postData(this.postData, "mark-as-read-notifications").then((result) =>{
      if(result) {
        this.loadNotifications();
      }
      else{
        console.log("Failed on mark as read all notifications");
      }
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    }, (err) => {
      console.log("Connection failed");
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    });
  }

}
