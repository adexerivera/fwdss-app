import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";

@IonicPage()
@Component({
  selector: 'page-change-password-modal',
  templateUrl: 'change-password-modal.html',
})
export class ChangePasswordModalPage {
  profile = {
    id:       '',
    password: '',
    password_repeat: '',
  };

  // Form variables
  changePasswordForm: FormGroup;
  submitted: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public loading: LoadingController, public authService: AuthServiceProvider, public formBuilder:FormBuilder, private toastCtrl:ToastController) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.profile = data;
    this.changePasswordForm = formBuilder.group({
      password: ['', Validators.compose([Validators.required])],
      password_repeat: ['', Validators.compose([Validators.required])],
    }, {validator: this.matchingPasswords('password', 'password_repeat')});
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];
      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordModalPage');
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  save() {
    this.submitted = true;
    if(this.changePasswordForm.valid) {
      let loading = this.loading.create();
      loading.present();
      let postData = {
        form : JSON.stringify({
          id: this.profile.id,
          password: this.profile.password,
          password_repeat: this.profile.password_repeat,
        })
      };
      this.authService.postData(postData, "change-password").then((result) =>{
        if(result) {
          this.viewCtrl.dismiss(this.profile);
          setTimeout(() => {
            loading.dismiss();
          }, 1000);
        }
        else{
          console.log("Failed");
          setTimeout(() => {
            loading.dismiss();
          }, 1000);
        }
      }, (err) => {
        console.log("Connection failed");
        this.presentToast("Error al cambiar la contraseña");
        setTimeout(() => {
          loading.dismiss();
        }, 1000);
      });
    }
  }
}
