import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";

@Component({
  selector: 'chat-popover',
  templateUrl: 'chatPopover.html',
})
export class ChatPopoverPage {

  userData = {
    id: "",
    token: "",
    is_admin: false,
    username: "",
    device_token: "",
    refresh_token: "",
    unread_chats: 0
  };

  postData = {
    page: 1,
    type: 'report',
    token: '',
    message_id: ''
  };

  constructor (public viewCtrl: ViewController, public navParams: NavParams,
    public authService: AuthServiceProvider) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userData = data;
    this.postData.message_id = this.navParams.get('id');
  }

  remove() {
    this.postData.token = this.userData.token;
    this.authService.postData(this.postData, "remove-report").then((result) =>{
      if(result) {
        this.viewCtrl.dismiss(result);
      }
      else{
        console.log("Failed remove report");
      }
    }, (err) => {
      console.log("Connection failed remove report");
    });
  }
}
