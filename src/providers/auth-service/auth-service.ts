import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/forkJoin';
import moment from 'moment';
import * as momentTz from 'moment-timezone';

// let apiUrl = 'http://127.0.0.1:8000/api/';
// let apiUrl = 'http://schedule.adexerivera.com/api/';
let apiUrl = 'http://app.fwdss.com/api/';

@Injectable()
export class AuthServiceProvider {

  userData = {
    token: "",
    username: "",
    device_token: "",
    refresh_token: "",
  };
  scheduleData = {
    token: "",
    refresh_token: "",
    start: "",
    end: "",
    customer : ''
  };
  googleSendData = {
    key: 'AIzaSyBSBKC2EFm4j6Z0QqdXNy57U3h4h5J2kQw',
    singleEvents: true,
    timeMin: '',
    timeMax: '',
    alwaysIncludeEmail: false,
  }

  constructor(public http: Http) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userData = data;
  }

  jsonToUrlEncoded(jsonString) {
    return Object.keys(jsonString).map(function(key){
      return encodeURIComponent(key) + '=' + encodeURIComponent(jsonString[key]);
    }).join('&');
  }

  postData(credentials, type) {
    return new Promise((resolve, reject) => {
      let body = this.jsonToUrlEncoded(credentials);
      let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
      this.http.post(apiUrl + type, body, {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          console.log('Auth Service Provider err', err);
          let errorMessage = JSON.parse(err._body).title;
          reject(errorMessage);
        });
    });
  }

  getSchedule() {
    this.userData = JSON.parse(localStorage.getItem("userData"));
    this.scheduleData.token = this.userData.token;
    this.scheduleData.refresh_token = this.userData.refresh_token;
    return new Promise((resolve, reject) => {
      let body = this.jsonToUrlEncoded(this.scheduleData);
      let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
      this.http.post(apiUrl + 'events', body, {headers: headers})
        .subscribe(res => {
          let result = this.parseEvents(res.json());
          resolve(result);
        }, (err) => {
          reject(err);
        });
    });
  }

  getGoogle() {
    return new Promise((resolve, reject) => {
      let params = this.jsonToUrlEncoded(this.googleSendData);
      let headers = new Headers();
      this.http.get("https://www.googleapis.com/calendar/v3/calendars/lqknb35jmbq5qa2pdrbtm9hf64%40group.calendar.google.com/events?" + params, {headers: headers})
        .subscribe(res => {
          let result = this.parseGoogleEvents(res.json());
          resolve(result);
        }, (err) => {
          console.log('Get google err');
          reject(err);
        });
    });
  }

  getEvents(scheduleParams, googleParams){
    this.scheduleData = scheduleParams;
    this.googleSendData = googleParams;
    return Observable.defer(()=>
      Observable.forkJoin(
        Observable.fromPromise(this.getSchedule()),
        Observable.fromPromise(this.getGoogle()))
    );
  }

  parseEvents(sevents) {
    var events = [];
    for (var i = 0; i < sevents.eventsData.length; i++) {
      var iniTime = momentTz.tz(sevents.eventsData[i].start_datetime, "Atlantic/Canary");
      var iniHour = iniTime.get('hours');
      var iniMinutes = iniTime.get('minutes');
      var endTime = momentTz.tz(sevents.eventsData[i].end_datetime, "Atlantic/Canary");
      var endHour = endTime.get('hours');
      var endMinutes = endTime.get('minutes');
      if(sevents.eventsData[i].all_day) {
        iniHour = 0;
        iniMinutes = 0;
        endHour = 23;
        endMinutes = 59;
      }
      events.push({
        id: sevents.eventsData[i].other_fields['id'],
        title: sevents.eventsData[i].title,
        startTime: new Date(
          iniTime.year(),
          iniTime.month(),
          iniTime.date(),
          iniHour,
          iniMinutes
        ),
        endTime: new Date(
          endTime.year(),
          endTime.month(),
          endTime.date(),
          endHour,
          endMinutes
        ),
        allDay: sevents.eventsData[i].all_day,
        bgColor: sevents.eventsData[i].bg_color,
        type: sevents.eventsData[i].other_fields['type'],
        customer: sevents.eventsData[i].other_fields['customer_id'],
        duration: endTime.diff(iniTime, 'minutes'),
        attendance: sevents.eventsData[i].other_fields['attendance'],
        observation: sevents.eventsData[i].other_fields['observation'],
      });
    }
    return events;
  }

  parseGoogleEvents(gevents) {
    var events = [];
    for (var i = 0; i < gevents.items.length; i++) {
      var iniTime = moment(gevents.items[i].start.date + 'T00:00:00Z');
      var endTime = moment(gevents.items[i].start.date + 'T23:59:59Z');
      events.push({
        id: gevents.items[i].id,
        title: gevents.items[i].summary,
        startTime: new Date(
          iniTime.year(),
          iniTime.month(),
          iniTime.date(),
          iniTime.hours(),
          iniTime.minutes()
        ),
        endTime: new Date(
          endTime.year(),
          endTime.month(),
          endTime.date(),
          endTime.hours(),
          endTime.minutes()
        ),
        allDay: true,
        bgColor: '#DD4B39',
        type: 'holiday',
      });
    }
    return events;
  }

}
