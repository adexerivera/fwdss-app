import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators/map';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

// let apiUrl = 'http://127.0.0.1:8000/api/';
// let apiUrl = 'http://schedule.adexerivera.com/api/';
let apiUrl = 'http://app.fwdss.com/api/';

export class ChatMessage {
  id: string;
  client_id: string;
  sender_id: string;
  sender_name: string;
  sender_admin: number;
  sender_avatar: string;
  receiver_id: string;
  receiver_name: string;
  receiver_admin: number;
  send: number | string;
  message: string;
  image: string;
  status: string;
  viewed: string;
  erased: boolean;
}

export class UserInfo {
  id: string;
  avatar?: string;
  username?: string;
}

export const sender_avatar = 'assets/imgs/logo.png';
export const receiver_avatar = 'assets/imgs/logo.png';

@Injectable()
export class ChatService {

  constructor(private http: HttpClient) {
  }

  jsonToUrlEncoded(jsonString) {
    return Object.keys(jsonString).map(function(key){
      return encodeURIComponent(key) + '=' + encodeURIComponent(jsonString[key]);
    }).join('&');
  }

  getMsgList(credentials): Observable<ChatMessage[]> {
    let body = this.jsonToUrlEncoded(credentials);
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.http.post<any>(apiUrl + 'reports', body, {headers: headers})
      .pipe(map(response => response.map(msg => ({
        ...msg,
        sender_avatar: sender_avatar
      }))));
  }

  sendMsg(msg: ChatMessage) {
    let postData = {
      msg: JSON.stringify(msg)
    };
    let body = this.jsonToUrlEncoded(postData);
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + 'report-msg', body, {headers: headers})
        .subscribe(res => {
          console.log('chat service resolve: ', res);
          resolve(res);
        }, (err) => {
          console.log('Auth Service Provider err on send message', err);
          let errorMessage = JSON.parse(err._body).title;
          reject(errorMessage);
        });
    });
  }

}
