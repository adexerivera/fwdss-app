import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, AlertController, Events, ToastController, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { SchedulePage } from '../pages/schedule/schedule';
import { CustomerPage } from '../pages/customer/customer';
import { NotificationPage } from '../pages/notification/notification';
import { ProfilePage } from '../pages/profile/profile';
import { SupportModalPage } from '../pages/support-modal/support-modal';
import { ReportPage } from '../pages/report/report';
import { ChatPage } from '../pages/chat/chat';
import { LegalNoticePage } from '../pages/legal-notice/legal-notice';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { AuthServiceProvider } from "../providers/auth-service/auth-service";
import { Device } from '@ionic-native/device';
import { Badge } from '@ionic-native/badge';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SchedulePage;

  pages: Array<{title: string, component: any, class: string}>;

  userData = {
    id: "",
    token: "",
    is_admin: false,
    reported: false,
    username: "",
    device_token: "",
    refresh_token: "",
    unread_reports: 0,
    unread_notifications: 0
  };

  constructor(private badge: Badge, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public loading: LoadingController, public push: Push, public alertCtrl: AlertController, public authService: AuthServiceProvider, private device: Device, public events: Events, public modalCtrl: ModalController, private toastCtrl:ToastController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Calendario', component: SchedulePage, class: 'fwdss-red' },
      { title: 'Usuarios', component: CustomerPage, class: 'fwdss-yellow' },
      { title: 'Notificaciones', component: NotificationPage, class: 'fwdss-yellow' },
      { title: 'Perfil', component: ProfilePage, class: 'fwdss-green' },
      { title: 'Listado Reportes', component: ReportPage, class: 'fwdss-green' },
      { title: 'Reportes', component: ChatPage, class: 'fwdss-green' }
    ];

    const data = JSON.parse(localStorage.getItem("userData"));
    if(data) {
      this.userData = data;
    }

    events.subscribe('user:info', () => {
      this.userInfo();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.events.publish('user:info');
      this.initPushNotification();
      this.initLocalNotifications();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page == 'Reportes') {
      this.nav.push(page.component);
    }
    else {
      this.nav.setRoot(page.component);
    }
  }

  badgeCount(page) {
    switch (page.title) {
      case 'Notificaciones': return (this.userData)? this.userData.unread_notifications : 0;
      case 'Listado Reportes': return (this.userData)? this.userData.unread_reports : 0;
      case 'Reportes': return (this.userData)? this.userData.unread_reports : 0;
    }
  }

  isAdminPage(page) {
    if(!this.userData)
      return false;
    switch (page.title) {
      case 'Usuarios': return (this.userData.is_admin)? '' + page.class : 'hide ' + page.class;
      case 'Listado Reportes': return (this.userData.is_admin)? '' + page.class : 'hide ' + page.class;
      case 'Reportes': return (this.userData.is_admin || !this.userData.reported)? 'hide ' + page.class : '' + page.class;
      default: return page.class;
    }
  }

  logout() {
    let loading = this.loading.create();
    loading.present();
    // Api Token Logout
    localStorage.clear();
    setTimeout(() => { this.nav.setRoot(LoginPage) }, 1000);
    setTimeout(() => { loading.dismiss(); }, 1000);
  }

  sendSupport() {
    let modal = this.modalCtrl.create(SupportModalPage);
    modal.present();
    modal.onDidDismiss(data => {
      if(data) {
        this.presentToast("Información enviada correctamente");
      }
    });
  }

  legalNotice() {
    this.nav.setRoot(LegalNoticePage);
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  initPushNotification() {
    if(!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available.');
      return;
    }

    this.push.hasPermission().then((res: any) => {
      if (res.isEnabled) {
        console.log('We have permission to send push notifications');
      } else {
        console.warn('We don\'t have permission to send push notifications');
      }
    });

    const options: PushOptions = {
      android: {
        senderID: '685522233864',
        icon: 'ic_notify',
        iconColor: '#2A3B49'
      },
      ios: {
        alert: 'true',
        badge: 'true',
        sound: 'true'
      },
      windows: {}
    }

    const pushObject: PushObject = this.push.init(options);

    // Push register
    pushObject.on('registration').subscribe((registration: any) => {
      let postData = {
        register : JSON.stringify({
          registrationid: registration.registrationId,
          manufacturer: this.device.manufacturer,
          model: this.device.model,
          platform: this.device.platform,
          uuid: this.device.uuid,
          version: this.device.version,
          username: this.userData.username
        })
      };
      this.authService.postData(postData, "register-device").then((result) =>{
        let registrationResponse: any = result;
        if(registrationResponse.result) {
          this.userData.device_token = registrationResponse.token;
          localStorage.setItem('userData', JSON.stringify(this.userData));
        }
      }, (err) => {
        console.warn("Error on register token");
      });
    });

    // Push notifications
    pushObject.on('notification').subscribe((notification: any) => {
      let notificationType = notification.additionalData.type;
      // Foreground
      if (notification.additionalData.foreground) {
        let activePage = this.nav.getActive().component.name;
        if(activePage != 'ChatPage') {
          let alert = this.alertCtrl.create({
            title: notification.title,
            message: notification.message,
            buttons: [
              {
                text: 'Ok',
                handler: () => {
                  this.notificaitonRedirect(notificationType);
                }
              }
            ]
          });
          alert.present();
        }
        else {
          if(notification.additionalData.report) {
            this.events.publish('chat:received', notification.additionalData.report);
          }
        }
      }
      else {
        // Background
        this.notificaitonRedirect(notificationType);
      }
      this.userInfo();
    });

    // Push error
    pushObject.on('error').subscribe(error => {
      console.error('Error with push plugin', error)
    });
  }

  notificaitonRedirect(type) {
    if(this.userData) {
      if(type == 'schedule') {
        this.nav.setRoot(NotificationPage);
      }
      else {
        if(this.userData.is_admin)
          this.nav.setRoot(ReportPage);
        else
          this.nav.setRoot(ChatPage);
      }
    }
    else {
      this.nav.setRoot(LoginPage);
    }
  }

  userInfo() {
    let userSet = JSON.parse(localStorage.getItem("userData"));
    if(userSet) {
      this.userData = userSet;
      this.authService.postData(this.userData, "user-info").then((result) =>{
        let userInfo: any = result;
        this.userData.unread_reports = userInfo.unreadReports;
        this.userData.unread_notifications = userInfo.unreadNotifications;
        localStorage.setItem('userData', JSON.stringify(this.userData));
        this.badge.set(userInfo.unreadReports + userInfo.unreadNotifications);
      }, (err) => {
        console.log("Connection failed");
      });
    }
  }

  initLocalNotifications() {
  }
}
