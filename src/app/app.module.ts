import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { SchedulePage } from '../pages/schedule/schedule';
import { CustomerPage } from '../pages/customer/customer';
import { LoginPage } from '../pages/login/login';
import { NotificationPage } from '../pages/notification/notification';
import { ProfilePage } from '../pages/profile/profile';
import { SupportModalPage } from '../pages/support-modal/support-modal';
import { GdprModalPage } from '../pages/gdpr-modal/gdpr-modal';
import { ReportPage } from '../pages/report/report';
import { ChatPage } from '../pages/chat/chat';
import { LegalNoticePage } from '../pages/legal-notice/legal-notice';
import { GalleryPage } from '../pages/gallery/gallery';

import { ChatPopoverPage } from '../pages/popovers/chatPopoverPage';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { NgCalendarModule } from 'ionic2-calendar';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { Push } from '@ionic-native/push';
import { Device } from '@ionic-native/device';
import { ChatService } from '../providers/chat-service/chat-service';

import { DatePickerModule } from 'ionic2-date-picker';
import {TimeAgoPipe} from 'time-ago-pipe';

import { SelectSearchableModule } from 'ionic-select-searchable';
import { Camera } from '@ionic-native/camera';
import { Badge } from '@ionic-native/badge';
import { IonicImageViewerModule } from 'ionic-img-viewer';

registerLocaleData(localeEs);

@NgModule({
  declarations: [
    MyApp,
    SchedulePage,
    CustomerPage,
    LoginPage,
    NotificationPage,
    ProfilePage,
    SupportModalPage,
    GdprModalPage,
    ReportPage,
    TimeAgoPipe,
    ChatPage,
    LegalNoticePage,
    GalleryPage,
    ChatPopoverPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    NgCalendarModule,
    DatePickerModule,
    SelectSearchableModule,
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SchedulePage,
    CustomerPage,
    LoginPage,
    NotificationPage,
    ProfilePage,
    SupportModalPage,
    GdprModalPage,
    ReportPage,
    ChatPage,
    LegalNoticePage,
    GalleryPage,
    ChatPopoverPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    Push,
    Device,
    { provide: LOCALE_ID, useValue: 'es' },
    ChatService,
    Badge,
    Camera
  ]
})
export class AppModule {}
