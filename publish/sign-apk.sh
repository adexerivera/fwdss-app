#!/bin/bash

# Fist you must make ionic cordova build --release android

APPNAME=${1}
PALTFORM=${2}

if [ "$APPNAME" == "" ]; then
    echo -e "Enter the app name"
    read appname
    APPNAME=$appname
fi

if [ "$SYSTEM" == "" ]; then
    echo -e "Enter the system linux or mac"
    read system
    SYSTEM=$system
fi

echo -e "******************************************************************************"
echo -e "Now you sign the ${APPNAME}"
echo -e "******************************************************************************"

BUILDDATE=`date +%Y%m%d`
BUILDTIME=`date +%H%M%S`

if [ "$SYSTEM" == "linux" ]; then
	mkdir "releases"
	mkdir "releases"/"${BUILDDATE}"
	mkdir "releases"/"${BUILDDATE}"/"${BUILDTIME}"
	$ANDROID_HOME/build-tools/27.0.2/zipalign -v -p 4 ../platforms/android/build/outputs/apk/android-release-unsigned.apk ${APPNAME}-unsigned-aligned.apk
	$ANDROID_HOME/build-tools/27.0.2/apksigner sign --ks android-apk-key.jks --out "releases"/${BUILDDATE}/${BUILDTIME}/${APPNAME}.apk ${APPNAME}-unsigned-aligned.apk
	$ANDROID_HOME/build-tools/27.0.2/apksigner verify "releases"/${BUILDDATE}/${BUILDTIME}/${APPNAME}.apk
else
	cd /Users/casa/Library/Android/sdk/build-tools/27.0.3/
	mv ${APPNAME}-unsigned-aligned.apk ${APPNAME}-unsigned-aligned.apk."${BUILDDATE}""${BUILDTIME}"
	./zipalign -v -p 4 /Users/casa/Projects/Ionic/fwdss-app/platforms/android/build/outputs/apk/android-release-unsigned.apk ${APPNAME}-unsigned-aligned.apk
	mkdir /Users/casa/Projects/Ionic/fwdss-app/publish/releases/"${BUILDDATE}"
	mkdir /Users/casa/Projects/Ionic/fwdss-app/publish/releases/"${BUILDDATE}"/"${BUILDTIME}"
	./apksigner sign --ks /Users/casa/Projects/Ionic/fwdss-app/publish/android-apk-key.jks --out /Users/casa/Projects/Ionic/fwdss-app/publish/releases/"${BUILDDATE}"/"${BUILDTIME}"/${APPNAME}.apk ${APPNAME}-unsigned-aligned.apk
	./apksigner verify /Users/casa/Projects/Ionic/fwdss-app/publish/releases/"${BUILDDATE}"/"${BUILDTIME}"/${APPNAME}.apk
fi

echo -e "******************************************************************************"
echo -e "Finish your releases/${BUILDDATE}/${BUILDTIME}/${APPNAME}.apk it's ready"
echo -e "******************************************************************************"
